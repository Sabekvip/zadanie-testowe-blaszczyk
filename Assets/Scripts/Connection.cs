﻿using System.Collections;
using UnityEngine;

//Class to check the mass of an object and its explosion
public class Connection : MonoBehaviour
{
    public Rigidbody rb;
    float maxMass;          //Max obejct mass
    public float onceMass;  //Mass of the starting object
    GameObject explodeObj;  //Objects of explosion


    private void Start()
    {
        explodeObj = Resources.Load<GameObject>("Sphere") as GameObject;
        onceMass = this.rb.mass;
        maxMass = this.rb.mass * 50;
    }
    //Function to detect collision
    private void OnCollisionEnter(Collision collision)
    {
        //Checking connect avalilable each object and collision object
        if (this.gameObject.GetComponent<SphereAvalilable>().connectionAvalilable == true &&
            collision.gameObject.GetComponent<SphereAvalilable>().connectionAvalilable == true)
        {
            //Object is assigned as a parent of the collision object
            collision.transform.SetParent(this.transform);

            if (transform.childCount > 0)
            {
                //assiging mass of the collision object to object
                this.rb.mass += this.transform.GetChild(0).GetComponent<Rigidbody>().mass;
                //Changing size of object
                ChangeSize(this.transform.GetChild(0).transform);
                //Destoy child of object
                Destroy(this.transform.GetChild(0).gameObject);
                //Check max mass of object and explode if reach max
                if (this.rb.mass >= maxMass) Explode();
            }
        }
    }

    //Function to change size of object
    void ChangeSize(Transform scale)
    {
        this.transform.localScale += scale.localScale / 2;
    }

    //Function to explode object if reach max mass
    void Explode()
    {
        //Number of objects in the explosion
        int count = (int)(this.rb.mass / onceMass);

        //Coroutines
        StartCoroutine(DestroyObject());
        StartCoroutine(ExplodeSphere());

        //Coroutine to destroy object 
        IEnumerator DestroyObject()
        {
            Destroy(this.gameObject);
            yield return new WaitForSeconds(0f);
        }

        //Corotuine to spawn objects in one place so that their collisions would cause movement to random places
        IEnumerator ExplodeSphere()
        {
            for (int i = 0; i < count; i++)
            {
                Vector3 explosePosition = new Vector3(this.transform.position.x + Random.Range(-0.1f, 0.1f), this.transform.position.y + Random.Range(-0.1f, 0.1f), this.transform.position.z + Random.Range(-0.1f, 0.1f));
                Quaternion exploseRotation = new Quaternion(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
                Instantiate(explodeObj, explosePosition, exploseRotation);
                //Enable switch until explosion
                explodeObj.GetComponent<SphereColliderSwitch>().enabled = true;
            }
            yield return new WaitForSeconds(0f);
        }




    }
}
