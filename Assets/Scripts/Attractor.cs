﻿using System.Collections.Generic;
using UnityEngine;

//Class to attract object each other
public class Attractor : MonoBehaviour
{
    public Rigidbody rb;
    float speed = 250;      //Extra speed of objects
    Vector3 force;          //Power with which objects are attracted
    int repuls;             //Kind of gravity (Attraction=1 or repulsion=-1)

    Counter counter;        //Instance of Counter - to get of maximum number
    GameObject counterObj;  //Object to find Counter

    //List of all objects with attract avalilable
    public static List<Attractor> Attractors;

    void Start()
    {
        repuls = 1;
        counterObj = GameObject.Find("Counter");
        counter = counterObj.GetComponent<Counter>();
    }
    private void FixedUpdate()
    {
        //Loop fo snapping objects
        foreach (Attractor item in Attractors)
        {
            if (item != this)
                Attract(item);
        }
    }

    void Update()
    {
        //Check maximum objects on map
        if (counter.GetComponent<Counter>().finish == true)
        {
            repuls = -1;
        }
    }

    private void OnEnable()
    {
        //Adding to list of objects with attract avalilable
        if (Attractors == null)
            Attractors = new List<Attractor>();

        Attractors.Add(this);
    }
    private void OnDisable()
    {
        Attractors.Remove(this);
    }

    //Function for calculating the force of attraction of objects
    void Attract(Attractor objAttract)
    {
        Rigidbody rbAttract = objAttract.rb;
        //Variable to define the direction of attracted objects
        Vector3 direction = rb.position - rbAttract.position;
        //Distance defined by the direction vector
        float distance = direction.magnitude;

        //Variable determining the power of attraction
        float forceMagnitude = speed * (rb.mass * rbAttract.mass) / Mathf.Pow(distance, 2);

        force = repuls * direction.normalized * forceMagnitude;


        rbAttract.AddForce(force);



    }



}
