﻿using System.Collections;
using UnityEngine;

//Class to change enabled object scripts and collision
public class SphereColliderSwitch : MonoBehaviour
{
    Collider collid;
    Attractor attractor;
    Connection connection;
    SphereAvalilable sphereAvalilable;

    void Start()
    {
        collid = this.GetComponent<Collider>();         //Collider's obiejct
        attractor = this.GetComponent<Attractor>();     //Attractor script
        connection = this.GetComponent<Connection>();   //Connection script
        sphereAvalilable = this.GetComponent<SphereAvalilable>();   //SphereAvalilable script

        //Coroutines
        StartCoroutine(TurnOfCollider());
        StartCoroutine(Swtich());

        //Coroutine to turn of collider on explode object
        IEnumerator TurnOfCollider()
        {
            yield return new WaitForSeconds(0.1f);
            if (collid.enabled == true)
                collid.enabled = !collid.enabled;
        }

        //Coroutine to disable collider and script for 0,5s to explode
        IEnumerator Swtich()
        {

            if (attractor.enabled == true)
                attractor.enabled = !attractor.enabled;
            if (connection.enabled == true)
                connection.enabled = !connection.enabled;
            sphereAvalilable.connectionAvalilable = false;
            yield return new WaitForSeconds(0.5f);
            collid.enabled = true;
            attractor.enabled = true;
            connection.enabled = true;
            sphereAvalilable.connectionAvalilable = true;
        }

    }

}
