﻿using System.Collections;
using UnityEngine;

//Class to spawn object on the map
public class SpawnPoint : MonoBehaviour
{
	public GameObject obj;  //Spawning object
	public int count;        //Count of spawned objects
	public int maxCount;    //Maximum objects on map
	Vector3 spawnPosition;  //Spawn object's position
	Quaternion spawnRotation; //Spawn object's rotation  

	private void Start()
	{
		//Defining the object from resources
		obj = Resources.Load<GameObject>("Sphere") as GameObject;
		maxCount = 250;
		count = 0;

		//Spawning coroutine
		StartCoroutine(Spawning());

		//Spawning object eacht 0,25s in random places on map
		IEnumerator Spawning()
		{
			for (int i = 0; i < maxCount; i++)
			{
				yield return new WaitForSeconds(0.25f);
				spawnPosition = new Vector3(Random.Range(-20, 20), Random.Range(-20, 20), Random.Range(-20, 20));
				spawnRotation = new Quaternion(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
				Instantiate(obj, spawnPosition, spawnRotation);
				obj.GetComponent<SphereColliderSwitch>().enabled = false;
				count++;
			}
			//Disable spawn point on end spawning
			this.gameObject.SetActive(false);
		}
	}



}
