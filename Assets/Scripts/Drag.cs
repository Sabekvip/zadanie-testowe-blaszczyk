﻿using UnityEngine;

//Class to define drag of object
public class Drag : MonoBehaviour
{
    public Rigidbody rb;

    void Update()
    {
        //Increasing drag over time - simulating air resistance
        if (this.rb.drag < 0.5)
            this.rb.drag += (float)0.0005;
    }
}
