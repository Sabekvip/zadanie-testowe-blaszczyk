﻿using UnityEngine;
using UnityEngine.UI;

//Class to count spawned object on map
public class Counter : MonoBehaviour
{
    SpawnPoint spawnPoint;  //Instance of SpawnPoint
    GameObject spawnObj;    //Object to find spawnPoint on map
    public bool finish = false;

    void Start()
    {
        spawnObj = GameObject.Find("SpawnPoint");
        spawnPoint = spawnObj.GetComponent<SpawnPoint>();
    }

    void Update()
    {
        //Changing text on counter
        this.GetComponent<Text>().text = spawnPoint.count.ToString();
        if (spawnPoint.count == spawnPoint.maxCount)
        {
            //Disable function if maximum is reached
            this.gameObject.GetComponent<Counter>().enabled = false;
            //Setting the end spawning flag
            finish = true;
        }
    }
}
